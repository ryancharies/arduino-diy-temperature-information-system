#include <Wire.h>
#include <LiquidCrystal_PCF8574.h> //memasukan library LCD
#include <DS3231.h> //memasukan library DS3231
#include <DHT.h>
#include <DallasTemperature.h>//measukan library DS18B20
#define DHTPIN 3
#define ONE_WIRE_BUS 2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

//-----------------------------------------------------------------//
#include <ArduinoJson.h>
#include <Ethernet.h>
#include <SPI.h>

EthernetClient client;
const char * server = "192.168.0.103";
const char * resource = "/trial/apiTemperatureInformation/api/pasien/sensor";
unsigned long lastConnectionTime = 0;           // last time you connected to the server, in milliseconds
const unsigned long postingInterval = 10*1000;  // delay between updates, in milliseconds
//byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte mac[] = {0xB0,0xCD,0xAE,0x0F,0xDE,0x10};
int device_id = 1;
String data;
float suhu_ruang;
float suhu_tubuh;
float kelembapan;
String tanggal;
//-----------------------------------------------------------------//

DS3231 rtc(SDA, SCL); //prosedur pembacaan I2C
LiquidCrystal_PCF8574 lcd(0x27); //didapat dari i2c scanner
OneWire oneWire(ONE_WIRE_BUS); //setup sensor DS18B20
DallasTemperature SuhuTubuh( & oneWire); // berikan nama variabel,masukkan ke pustaka Dallas
int layar = 0;
float SuhuTubuhSekarang;

void setup() {    
    //set komunikasi baut di serial monitor pada kecepatan 115200
    Serial.begin(115200);
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  
    Serial.println("Initialize Ethernet with DHCP:");
    if (Ethernet.begin(mac) == 0) {
      Serial.println("Failed to configure Ethernet using DHCP");
      // Check for Ethernet hardware present
      if (Ethernet.hardwareStatus() == EthernetNoHardware) {
        Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
        while (true) {
          delay(1); // do nothing, no point running without Ethernet hardware
        }
      }
      if (Ethernet.linkStatus() == LinkOFF) {
        Serial.println("Ethernet cable is not connected.");
      }
      // try to congifure using IP address instead of DHCP:
      Ethernet.begin(mac);
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
    } else {
      Serial.print("DHCP assigned IP ");
      Serial.println(Ethernet.localIP());
    }
    // give the Ethernet shield a second to initialize:
    delay(1000);
  
    //menuliskan data di serial monitor
    Serial.println("TUGAS AKHIR TT 2019");
    Serial.println("SMART NURSE");
    SuhuTubuh.begin();
    dht.begin();
    //prosedur pembacaan RTC DS3231
    rtc.begin();

    //Inisialisasi LCD berukuran 16 kolom dan 2 baris
    lcd.begin(16, 2);
    //menyalakan backlight LCD, jika mematikan dapat diisi dengan nilai "0"
    lcd.setBacklight(1);

    lcd.setCursor(3, 0);
    lcd.print("TUGAS AKHIR"); //pada baris pertama

    lcd.setCursor(3, 1);
    lcd.print("SMART NURSE"); //pada baris kedua dituliskan LCD

    delay(5000); //Waktu jeda
    lcd.clear();
}

float ambilSuhu() {
    SuhuTubuh.requestTemperatures();
    float suhu = SuhuTubuh.getTempCByIndex(0);
    return suhu;
}

void loop() {
    delay(2500);
    lcd.clear();
    
    SuhuTubuhSekarang = ambilSuhu();
    suhu_tubuh = SuhuTubuhSekarang;
   
    float h = dht.readHumidity();
    kelembapan = h;
    // Read temperature as Celsius
    float t = dht.readTemperature();
    suhu_ruang = t;
    // Read temperature as Fahrenheit
    float f = dht.readTemperature(true);

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t) || isnan(f)) {
        Serial.println("Failed to read from DHT sensor!");
        return;
    }

    // Compute heat index
    // Must send in temp in Fahrenheit!
    float hi = dht.computeHeatIndex(f, h);

    Serial.print("Hum: ");
    Serial.print(h);
    Serial.println(" %");
    Serial.print("Temp: ");
    Serial.print(t);
    Serial.println(" *C");
    Serial.print("Body temp: ");
    Serial.println(SuhuTubuhSekarang);
    
    Serial.println();
    
    tanggal = (String)rtc.getDateStr() + " " + (String)rtc.getTimeStr();
    Serial.println(tanggal);

    if (layar == 0) {
        lcd.setCursor(0, 0);
        lcd.print("Hum");
        lcd.setCursor(7, 0);
        lcd.print(h);
        lcd.setCursor(0, 1);
        lcd.print("Temp");
        lcd.setCursor(7, 1);
        lcd.print(t);
        layar = 1;
    } else {
        if (layar == 1) {
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print(rtc.getDOWStr(FORMAT_SHORT)); //prosedur pemanggilan hari

            lcd.setCursor(4, 0);
            lcd.print(rtc.getDateStr()); //prosedur pemanggilan tanggal
            lcd.setCursor(0, 1);
            lcd.print(rtc.getTimeStr()); //prosedur pemanggilan waktu
            layar = 2;
        } else {
            lcd.setCursor(0, 0);
            lcd.print("Body temp ");
            lcd.setCursor(11, 0);
            lcd.print(SuhuTubuhSekarang);
            layar = 0;
        }
    }

  if (client.available()) {
    char c = client.read();
    Serial.write(c);
  }
  if (millis() - lastConnectionTime > postingInterval) {
    httpPost();
  }
    
}

// Open connection to the HTTP server
void httpPost() {
  client.stop();
  
  data = "suhu_ruang=";
  data.concat(suhu_ruang);
  data.concat("&suhu_tubuh=");
  data.concat(suhu_tubuh);
  data.concat("&kelembapan=");
  data.concat(kelembapan);
  data.concat("&tanggal=");
  data.concat(tanggal);
  data.concat("&id_alat=");
  data.concat(device_id); 
  
  if (client.connect(server, 80)) {
    Serial.println("POST data to server");
    client.print("POST ");
    client.print(resource);
    client.println(" HTTP/1.1");
    client.print("Host: ");
    client.println(server); 
    client.println("Content-Type: application/x-www-form-urlencoded");
    client.print("Content-Length: ");
    client.println(data.length());
    client.println();
    client.print(data);

    // note the time that the connection was made:
    lastConnectionTime = millis();
  } else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
  }
}
